﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace FakeDoc
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void DelExe()
        {
            string[] files = Directory.GetFiles(textBox1.Text, "*.exe", SearchOption.AllDirectories);

            label1.Text = "Поиск и удаление вирусов";
            Application.DoEvents();
            progressBar1.Maximum = files.Length;
            int pr = 0;

            foreach (string PathFile in files)
            {
                try
                {
                    File.Delete(PathFile);
                    textBox2.Text += string.Format("Удалено", Path.GetFileName(PathFile));
                }
                catch (Exception ex)
                {
                    textBox2.Text += ex.Message + "\r\n";
                }


                progressBar1.Value = ++pr;
            }
        }

        private void recDir()
        {
            string[] filesAll = Directory.GetDirectories(textBox1.Text,"*",SearchOption.AllDirectories);

            label1.Text = "Востановление Каталогов";
            Application.DoEvents();

            progressBar1.Maximum = filesAll.Length;
            int pr = 0;

            foreach (string PathFile in filesAll)
            {
                try
                {
                    DirectoryInfo di = new DirectoryInfo(PathFile);
                    di.Attributes = FileAttributes.Normal;

                }
                catch (Exception ex)
                {
                    textBox2.Text += ex.Message + "\r\n";
                }


                progressBar1.Value = ++pr;
            }



        }

        private void recFile()
        {
            string[] filesAll = Directory.GetFiles(textBox1.Text, "*.*", SearchOption.AllDirectories);

            label1.Text = "Востановление файлов";
            Application.DoEvents();
            progressBar1.Maximum = filesAll.Length;
            int pr = 0;

            foreach (string PathFile in filesAll)
            {
                try
                {

                    File.SetAttributes(PathFile, FileAttributes.Normal);
                    textBox2.Text += string.Format("Найдено ", Path.GetFileName(PathFile));
                }
                catch (Exception ex)
                {
                    textBox2.Text += ex.Message + "\r\n";
                }

                progressBar1.Value = ++pr;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(textBox1.Text))
            {
                DelExe();
                recDir();
                recFile();

                label1.Text = "Готово";
            }
            else textBox2.Text += "Неправильно указанна директория"; 
        }
    }
}